
function Tire(aType, aSize, aInstal, aLoadIndex, aLoadMax, aPressure){
    this.transport_type = aType;
    this.tireSize = aSize;
    this.instal = aInstal;
    this.loadIndex = aLoadIndex;
    this.loadMax=aLoadMax;
    this.tirePressure = aPressure;
}
var myLat = 0, myLong = 0;
// Wait for device API libraries to load
document.addEventListener("deviceready", onDeviceReady, false);
var watchID = null;

function checkConnection() {
    var networkState = navigator.network.connection.type;
    var states = {};
    states[Connection.UNKNOWN]  = 'Unknown connection';
    states[Connection.ETHERNET] = 'Ethernet connection';
    states[Connection.WIFI]     = 'WiFi connection';
    states[Connection.CELL_2G]  = 'Cell 2G connection';
    states[Connection.CELL_3G]  = 'Cell 3G connection';
    states[Connection.CELL_4G]  = 'Cell 4G connection';
    states[Connection.NONE]     = 'No network connection';
    return (states[networkState]);
}


// device APIs are available
function onDeviceReady() {
    var options = { timeout: 300000 };
    watchID = navigator.geolocation.getCurrentPosition(onSuccess, onError, options);
    //navigator.geolocation.clearWatch(watchID);
    $("#yes_close").on("vmousedown", function () {
        navigator.app.exitApp();
    });
    $("#dont_close").on("vmousedown", function () {
        $('#exit').dialog('close');
    });

    //getting amount of app launches:
    var launches = localStorage.getItem("launches");
    launches = launches != "" ? Number(launches) : 0;
    launches++;
    localStorage.setItem("launches", launches);

    //getting platform info:
    var platform = device.platform;
    localStorage.setItem("platform", platform);

    //checking network connection:
    var state = checkConnection();
    if (state == 'No network connection')
        $.mobile.changePage( "#noInternetConnection", { role: "dialog" } );
}

// onSuccess Geolocation
function onSuccess(position) {
    var element = document.getElementById('geolocation');
    myLat = position.coords.latitude;
    myLong = position.coords.longitude;
}


// onError Callback receives a PositionError object
function onError(error) {
    $("#loadFail div[role=main]").text("Ошибка определения вашей геопозиции: " + error.message);
    $.mobile.changePage( "#loadFail", { role: "dialog" } );
    $("#loadFail div[data-role=header] a").on("vmousedown", function(event){
        $("#loadFail div[role=main]").text("");
        event.preventDefault();
        event.stopImmediatePropagation();
        $.mobile.changePage( "#main");
    });
}


$(document).on('pageinit', "[data-role=page]", function() {
//loading updated data from server and saving in local storage:
    $.ajax({
        url: "http://tiresnode-smartytires.7e14.starter-us-west-2.openshiftapps.com/truck_tires_catalog",
        dataType: "json",
        success: function (data) {
            //saving update to local storage:
            var dataJSON = JSON.stringify(data);
            localStorage.setItem("truck_tires", dataJSON);
        }
    });

    $.ajax({
        url: "http://tiresnode-smartytires.7e14.starter-us-west-2.openshiftapps.com/trailer_tires_catalog",
        dataType: "json",
        success: function (data) {
            //saving update to local storage:
            var dataJSON = JSON.stringify(data);
            localStorage.setItem("trailer_tires", dataJSON);
        }
    });

    $.ajax({
        url: "http://tiresnode-smartytires.7e14.starter-us-west-2.openshiftapps.com/truckforces",
        dataType: "json",
        success: function (data) {
            //saving update to local storage:
            var dataJSON = JSON.stringify(data);
            localStorage.setItem("truckforces", dataJSON);
        }
    });
});

//actions on Pressure measure page:
$("#pressure").bind('pageshow', function() {
    //setting initial status of form elements:
    $( "#size" ).selectmenu( "option", "disabled", true );
    // $("input[value=single]").checkboxradio( "option", "disabled", true );
    // $("input[value=double]").checkboxradio( "option", "disabled", true );
    $( "#load_index" ).selectmenu( "option", "disabled", true );

    var truck_tires_catalog  = localStorage.getItem("truck_tires");
    if (truck_tires_catalog) {
        truck_tires_catalog = JSON.parse(truck_tires_catalog);
    }
    else{
        $("#loadFail div[role=main]").text("Ошибка загрузки каталога грузовых шин. Проверьте наличие подключения Интернет, или попробуйте еще раз через 1 минуту.");
        $.mobile.changePage( "#loadFail", { role: "dialog" } );
        $("#loadFail div[data-role=header] a").on("vmousedown", function(event){
            $("#loadFail div[role=main]").text("");
            event.preventDefault();
            $.mobile.changePage( "#main");
        });
        return;
    }

    var trailer_tires_catalog  = localStorage.getItem("trailer_tires");
    if (trailer_tires_catalog) {
        trailer_tires_catalog = JSON.parse(trailer_tires_catalog);
    }
    else{
        $("#loadFail div[role=main]").text("Ошибка загрузки каталога шин для прицепов. Проверьте наличие подключения Интернет, или попробуйте еще раз через 1 минуту.");
        $.mobile.changePage( "#loadFail", { role: "dialog" } );
        $("#loadFail div[data-role=header] a").on("vmousedown", function(event){
            $("#loadFail div[role=main]").text("");
            event.preventDefault();
            event.stopImmediatePropagation();
            $.mobile.changePage( "#main");
        });
        return;
    }


    //a function to filter array and live only unique elements
    function unique(arr) {
        var obj = {};

        for (var i = 0; i < arr.length; i++) {
            var str = arr[i];
            obj[str] = true; // запомнить строку в виде свойства объекта
        }

        return Object.keys(obj); // или собрать ключи перебором для IE8-
    }


    var source = []; //source catalog according to user's tire type choice

    //user's choice data:
    var type, sz, instal, load_index, axleLoad;
    var catalog_objs = []; //array to save all tires from catalog with type and size, chosen by user

    //adding events on pressure_data forms fields change
    $("input[name=truckType]").change(function (event) {
        event.stopImmediatePropagation();
        type = $("input[name=truckType]:checked").val();
        localStorage.setItem("type", type);
        if (!type)
            return;
        //getting all available tire sizes for selected transport type:
        source = type == 'truck' ? truck_tires_catalog : trailer_tires_catalog;
        $("#size option").remove(':not(:first-child)'); //if other tire type had already been chosen before
        var sizesList = [];
        $(source).each(function () {
            sizesList.push($(this).prop('tireSize'));
        });
        sizesList = unique(sizesList);
        //adding available tire sizes to the select as options:
        $(sizesList).each(function () {
            $("<option/>").text(this).val(this).appendTo($("#size"));
        });
        $("#size").selectmenu( "option", "disabled", false );
        $("#size").selectmenu('refresh', true);

        //clearing all data, that could be chosen before:
        $("input[value=single]").checkboxradio( "option", "disabled", true)
            .prop('checked', false)
            .checkboxradio('refresh');
        $("input[value=double]").checkboxradio( "option", "disabled", true)
            .prop('checked', false)
            .checkboxradio('refresh');
        $("#load_index option").remove(':not(:first-child)');
        $("#load_index").selectmenu('refresh');
        sz = instal = load_index = undefined;
    });

    //adding event listener on size select to make load properties available:
    $("#size").change(function(event, ui) {
        event.stopImmediatePropagation();
        sz = $("#size option:selected").val();
        localStorage.setItem("size", sz);
        //finding all tires from catalog with type and size, chosen by user:
        catalog_objs = source.filter(function (item) {
            return item.tireSize == sz;
        });

        //checking, if different types of installation are available:
        var flag_single = false,
            flag_double = false;
        $(catalog_objs).each(function () {
            if (this.instal == "single")
                flag_single = true;
            else if (this.instal == "double")
                flag_double = true;
        });
        $("input[value=single]").checkboxradio( "option", "disabled", !flag_single )
                .prop('checked', false)
                .checkboxradio('refresh');
        $("input[value=double]").checkboxradio("option", "disabled", !flag_double)
                .prop('checked', false)
                .checkboxradio('refresh');
        $("#load_index option").remove(':not(:first-child)');
        $("#load_index").selectmenu('refresh');
        instal = load_index = undefined;
    });

    $("input[name=installation]").change(function (event) {
        event.stopImmediatePropagation();
        instal = $("input[name=installation]:checked").val();
        localStorage.setItem("instal", instal);
        //getting all values of load indexes for this size:
        var load_indexes = [];
        $(catalog_objs).each(function () {
            if (this.instal == instal)
                load_indexes.push(this.loadIndex);
        });
        $("#load_index option").remove(':not(:first-child)'); //if other install types had already been chosen before
        //adding available load indexes to the select as options:
        $(load_indexes).each(function () {
            $("<option/>").text(this).val(this).appendTo($("#load_index"));
        });
        $("#load_index").selectmenu( "option", "disabled", false );
        $("#load_index").selectmenu('refresh', true);
    });

    $("#load_index").change(function (event) {
        event.stopImmediatePropagation();
        load_index = $("#load_index option:selected").val();
        localStorage.setItem("load_index", load_index);
    });

    $("input[name=axleLoad]").on("input", function () {
        axleLoad = Number($("input[name=axleLoad]").val());
        localStorage.setItem("axleLoad", axleLoad);
    });

    $("#get_result").on("vclick", function (event) {
        event.stopImmediatePropagation();
        type = localStorage.getItem("type");
        sz = localStorage.getItem("size");
        instal = localStorage.getItem("instal");
        load_index = localStorage.getItem("load_index");
        axleLoad = Number(localStorage.getItem("axleLoad"));
        var optiPressure = 0;
        var usersTire = new Tire(type, sz, instal, load_index);
        var catalogTire = 0;
        catalog_objs.forEach(function (item, i) {
            if (item.instal == usersTire.instal &&
                Number(item.loadIndex) == Number(usersTire.loadIndex)) {
                catalogTire = item;
                return false;
            }
        });

        if (axleLoad > Number(catalogTire.loadMax))
        {
            event.preventDefault();
            event.stopImmediatePropagation();
            $("#overLoad div[role=main]").text("Ваша нагрузка на ось превышает допустимое значение (" +
                catalogTire.loadMax + " кг)");
            $.mobile.changePage( "#overLoad", { role: "dialog" } );
            return;
        }

        //defining an optimal pressure:
        var prev_pressure = 5, //initialized with min pressure
            prev_load = 0;
        for (var key in catalogTire.tirePressure)
        {
            var current_pressure = Number(key);
            var current_load = Number(catalogTire.tirePressure[key]);
            if (prev_load <= axleLoad && current_load >= axleLoad)
            {
                var mid_load = (prev_load + current_load) / 2;
                optiPressure = (axleLoad < mid_load) ? prev_pressure : current_pressure;
                localStorage.setItem("optiPressure", optiPressure);
                break;
            }
            prev_pressure = current_pressure;
            prev_load = current_load;
        }

        event.preventDefault();
        $.mobile.changePage( "#result", { role: "dialog" } );
        //filling in form results:
        $("#result p:first-of-type>span ")
            .text(type == 'truck' ? "Грузовые автомобили, тягачи, автобусы" :
                type == 'trailer' ? 'Прицепы и полуприцепы' : 'Не определен');
        $("#result p:nth-of-type(2)>span").text(sz ? sz : "не определен");
        $("#result p:nth-of-type(3)>span")
            .text(instal == 'single' ? 'одиночная' : instal == 'double' ? 'сдвоенная'
                    : 'не определена');
        $("#result p:nth-of-type(4)>span")
            .text(load_index ? load_index : "не определен");
        $("#result p:nth-of-type(5)>span").text(axleLoad ? axleLoad : "не определена");
        $("#result p:nth-of-type(6)>span").text(localStorage.getItem("optiPressure") != "" ? localStorage.getItem("optiPressure") : "не определено");
        $("#result").bind("pagehide",function() {
            var launches = localStorage.getItem("launches");
            launches = launches ? Number(launches) : 0;
            var users_reaction = localStorage.getItem("users_reaction");
            if (launches >= 5 && (users_reaction == "later" || !users_reaction)) {
                $.mobile.changePage("#rate", {role: "dialog"});
                $("#yes_rate").on("vclick", function (event) {
                    localStorage.setItem("users_reaction", "rated");
                    var platform = localStorage.getItem("platform");
                    if (platform == "Android")
                        $("#yes_rate").attr("href", "https://www.android.com/");
                    else
                        $("#yes_rate").attr("href", "http://windowsphone.com.ua/");
                    $("#rate").dialog("close");
                });
                $("#rate_later").on("vclick", function (event) {
                    localStorage.setItem("users_reaction", "later");
                    $("#rate").dialog("close");
                });
                $("#never_rate").on("vclick", function (event) {
                    localStorage.setItem("users_reaction", "never_rate");
                    $("#rate").dialog("close");
                });
            }
        });

       // clearing form for next query:
        $("input[name=truckType]").prop('checked', false)
            .checkboxradio('refresh');
        $("#size option").remove(':not(:first-child)');
        $( "#size" ).selectmenu( "option", "disabled", true )
            .selectmenu('refresh');
        $("input[value=single]").checkboxradio( "option", "disabled", true)
            .prop('checked', false)
            .checkboxradio('refresh');
        $("input[value=double]").checkboxradio( "option", "disabled", true)
            .prop('checked', false)
            .checkboxradio('refresh');
        $("#load_index option").remove(':not(:first-child)');
        $("#load_index").selectmenu( "option", "disabled", true )
            .selectmenu('refresh');
        $("input[name=axleLoad]").val(0).textinput('refresh');
        //cleaning data:
        type = sz = instal = load_index = axleLoad = undefined;
        source = catalog_objs = [];
    });
});


function Truckforce(aName, aAddress, aPhone, aWorktime, aLat, aLng, aDist) {
    this.name = aName;
    this.address = aAddress;
    this.phone = aPhone;
    this.worktime = aWorktime;
    this.lat = aLat;
    this.lng = aLng;
    this.distance = aDist;
}



//binding event on map page:
$("#service").bind('pageshow', initMap);
$("#show_map").on("vmousedown", function () {
    localStorage.setItem("map_opened", "true");
});

function initMap(){
    var heightScreen = $(window).height();
    var widthScreen = $(window).width();
    var headerHeight = $("#service>div[data-role=header]").height();
    var footerHeight = $("#service>div[data-role=footer]").height();

    $('#map').css({'width':widthScreen,'height':heightScreen - headerHeight - footerHeight});

    var basic_latlng = new google.maps.LatLng(myLat, myLong);

    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 8,
        center: basic_latlng
    });

    var truck_marker = new google.maps.Marker({
        position: {lat: myLat, lng: myLong},
        icon: "img/truck.png",
        map: map,
        animation: google.maps.Animation.DROP

    });
    $('#my_coord').on('vmousedown', function () {
        map.setCenter(basic_latlng);
        truck_marker.setAnimation(google.maps.Animation.BOUNCE);
        setTimeout(function () {
            truck_marker.setAnimation(null);
        }, 1000);
    });

    //ajax request to get JSON file with truckforces names and addresses:
    var truckforces = [];
    var points = [];
    var infowindows = [];
    var data = localStorage.getItem("truckforces");
    if (data){
        data = JSON.parse(data);
        $(data).each(function (i) {
            var contentString = data[i].name + '<br>' + data[i].address + '<br>' +  data[i].phone + '<br>' + data[i].worktime;
            var marker = new google.maps.Marker({
                position: {lat: data[i].lat, lng: data[i].lng},
                label: data[i].name
            });
            var truckforce_latlng = new google.maps.LatLng(data[i].lat, data[i].lng);
            //calculating a distance between truck and current truckforce:
            var distance = (google.maps.geometry.spherical.computeDistanceBetween(basic_latlng, truckforce_latlng) / 1000).toFixed(2);
            var infowindow = new google.maps.InfoWindow({
                content: contentString
            });
            infowindows.push(infowindow);
            marker.addListener('click', function () {
                infowindow.open(map, marker);
            });
            points.push(marker);
            truckforces.push(new Truckforce(data[i].name, data[i].address, data[i].phone, data[i].worktime, data[i].lat, data[i].lng, Number(distance)));
        });
        // Add a marker clusterer to manage the markers.
        var markerCluster = new MarkerClusterer(map, points,
            {
                imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m',
                maxZoom: 8
            });
        $('#service>div[data-role=footer]').text("").append("<ul/>");
        //sorting truckforces by the distance from client's truck:
        truckforces.sort(function (a, b) {
            return a.distance > b.distance;
        });
        //appending sorted list to footer:
        $(truckforces).each(function (i) {
            $('<li/>')
                .css("cursor", "pointer")
                .text(truckforces[i].distance + "км " + truckforces[i].name)
                .appendTo('#service>div[data-role=footer]>ul')
                .on('vmousedown', function () {
                    var pointIndex = -1;
                    //searching for the map point for clicked truckforce:
                    $(points).each(function(j){
                        if(points[j].label == truckforces[i].name)
                        {
                            pointIndex = j;
                            return false;
                        }
                    });
                    if (pointIndex != -1)
                    {
                        map.setCenter(points[pointIndex].position);
                        points[pointIndex].setAnimation(google.maps.Animation.BOUNCE);
                        setTimeout(function () {
                            points[pointIndex].setAnimation(null);
                            infowindows[pointIndex].open(map, points[pointIndex]);
                        }, 500);
                    }
                });
        });
    }
    else{
        var map_opened = localStorage.getItem("map_opened");
        if (map_opened == "true") {
            localStorage.setItem("map_opened", "false");
            $("#loadFail div[role=main]").text("Ошибка загрузки данных о сервисных станциях. Проверьте наличие подключения Интернет, или попробуйте еще раз через 1 минуту.");
            $.mobile.changePage("#loadFail", {role: "dialog"});
            $("#loadFail div[data-role=header] a").on("vmousedown", function (event) {
                $("#loadFail div[role=main]").text("");
                event.preventDefault();
                event.stopImmediatePropagation();
                $.mobile.changePage("#main");
            });
        }
    }
}

//damage page:
$("#damage").bind('pageshow', function () {
    var footerHeight = $("#damage>div[data-role=footer]").height();
    $("#damage>div[data-role=content]").css("padding-bottom", footerHeight + 10 +"px");

    //photo popup turning on on the 'damage' page:
    $(".photopopup").on({
        popupbeforeposition: function () {
            var maxHeight = $(window).height() - 60 + "px";
            $(".photopopup img").css("max-height", maxHeight);
        }
    });
});

